<?php

/*  ----------------------------------------------------------------------------
    Text logo + ad spot
 */

?>
<!-- text logo and ad -->
<div class="td-header-bg">
    <div class="container td-logo-rec-wrap">
        <div class="row">
            <div class="span9 header-logo-wrap" role="banner" itemscope="itemscope" itemtype="<?php echo td_global::$http_or_https?>://schema.org/Organization">
                <div class="td-logo-wrap-align">
                    <a itemprop="url" class="td-logo-wrap" href="<?php echo home_url(); ?>">
                        <span class="td-logo-text"><?php echo get_bloginfo( 'name' ); ?></span>
                        <span class="td-tagline-text"><?php echo get_bloginfo( 'description' ); ?></span>
                    </a>
                </div>
                <meta itemprop="name" content="<?php bloginfo('name')?>">
            </div>

            <div class="span1" id="td-top-search">
                <div class="header-search-wrap">
                    <div class="dropdown header-search">
                        <a id="search-button" href="#" role="button" class="dropdown-toggle " data-toggle="dropdown"><span class="td-sp td-sp-ico-search"></span></a>
                        <div class="dropdown-menu" aria-labelledby="search-button">
                            <form role="search" method="get" class="td-search-form" action="<?php echo home_url( '/' ); ?>">
                                <div class="td-head-form-search-wrap">
                                    <input class="needsclick" id="td-header-search" type="text" value="<?php echo get_search_query(); ?>" name="s" autocomplete="off" /><input class="wpb_button wpb_btn-inverse btn" type="submit" id="td-header-search-top" value="<?php _etd('Search')?>" />
                                </div>
                            </form>
                            <div id="td-aj-search"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="span2 cabecerasidebar">
                <?php if ( !function_exists('dynamic_sidebar') ||
                           !dynamic_sidebar('cabecera') ) : ?>
                            <p>no hay nada</p>
                <?php endif; ?>
                <?php
                // show the header ad spot
                echo td_global_blocks::get_instance('td_ad_box')->render(array('spot_id' => 'header'));
                ?>
            </div>
        </div>
    </div>
</div>
