<?php

class td_module_6 extends td_module {

    function __construct($post) {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render() {
        ob_start();
        ?>

        <div class="td_mod6 td_mod_wrap <?php echo $this->get_no_thumb_class();?>" <?php echo $this->get_item_scope();?>>
            <div class="fecha">
                 <i class="fa fa-tag"></i>
                <?php echo $this->get_category();?>
                <?php echo $this->get_commentsAndViews(); ?>
            </div>
        <?php echo $this->get_image('art-big-1col');?>
        <div class="titular">
            <?php echo $this->get_title(td_util::get_option('tds_mod6_title_excerpt'));?>
        </div>

        <div class="meta-info">
            <?php //echo $this->get_author();?>
            <?php //echo $this->get_date();?>
            <?php //echo $this->get_commentsAndViews();?>
        </div>


        <?php echo $this->get_item_scope_meta();?>
        </div>

        <?php return ob_get_clean();
    }
}