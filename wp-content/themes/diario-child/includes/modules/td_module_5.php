<?php

class td_module_5 extends td_module {

    function __construct($post) {
        //run the parrent constructor
        parent::__construct($post);
    }

    function render() {
        ob_start();
        ?>

        <div class="td_mod5 td_mod_wrap <?php echo $this->get_no_thumb_class();?>" <?php echo $this->get_item_scope();?>>
            <div class="cat_on_thumb">
                <?php // echo $this->get_tags(); ?>
            </div>
            <div class="fecha">
                 <i class="fa fa-tag"></i>
                <?php echo $this->get_category();?>
                <?php //echo $this->get_date(); ?>
                <?php echo $this->get_commentsAndViews(); ?>
            </div>
            <?php echo $this->get_image('modulo5');?>
            <?php echo $this->get_title(td_util::get_option('tds_mod5_title_excerpt'));?>


            <div class="meta-info">
                <?php //echo $this->get_author();?>
                <?php //echo $this->get_date();?>
                <?php //echo $this->get_commentsAndViews();?>
            </div>

            <div class="td-post-text-excerpt">
                <?php echo $this->get_excerpt(td_util::get_option('tds_mod5_content_excerpt'));?>
                 <a href="<?php echo $this->href?>"><i class="fa fa-plus-square"></i></a>
            </div>

            <?php echo $this->get_item_scope_meta();?>
        </div>

        <?php return ob_get_clean();
    }
}