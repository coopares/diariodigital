<?php
$vc_layout_sub_controls = array(
  array( 'link_post', __( 'Link to post', __PW_POST_LAYOUT_TEXTDOMAN__  ) ),
  array( 'no_link', __( 'No link', __PW_POST_LAYOUT_TEXTDOMAN__  ) ),
  array( 'link_image', __( 'Link to bigger image', __PW_POST_LAYOUT_TEXTDOMAN__  ) )
);

vc_map( array(
            "name" => __("PW News Ticker", __PW_POST_LAYOUT_TEXTDOMAN__ ),
            "description" => __("News Ticker Post Layout", __PW_POST_LAYOUT_TEXTDOMAN__ ),
            "base" => "pw_vc_marquee",
            "class" => "",
            "controls" => "full",
            "icon" => PW_PS_PL_URL_MARQUEE.'/assets/images/icons/marquee.png',
            "category" => __('PW Post Layout', __PW_POST_LAYOUT_TEXTDOMAN__ ),
            "params" => array(
							array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => __("Title", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_title",
								"value" => '',
								"description" => __("Enter Title.", __PW_POST_LAYOUT_TEXTDOMAN__ )
							),
							array(
								'type' => 'loop',
								'heading' => __( 'Your Query', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'param_name' => 'pw_query',
								'settings' => array(
								  'size' => array( 'hidden' => false, 'value' => 10 ),
								  'order_by' => array( 'value' => 'date' ),
								),
								'description' => __( 'Create WordPress loop, to populate content from your site.', __PW_POST_LAYOUT_TEXTDOMAN__ )
							),
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Link Target",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_link_target",
								"value" =>array(
									__( 'Same window', __PW_POST_LAYOUT_TEXTDOMAN__ ) => '_self',
									__( 'New window', __PW_POST_LAYOUT_TEXTDOMAN__ ) => "_blank"
									),
								"description" => __("Choose Link target", __PW_POST_LAYOUT_TEXTDOMAN__ )
							),						
							
							/*marquee*/
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Type Of Move",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_type",
								"value" =>array(
									__("Horizontal Move", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"horizontal",
									__("Vertical Move", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"vertical",
									__("Fade", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"fade"
									),
								"description" => __("Choose News Ticker type", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Position",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_position",
								"value" =>array(
									__("Current Place", __PW_POST_LAYOUT_TEXTDOMAN__ ) =>"none",
									__("Fix to Top of the Page", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"pl-fixedticker-top",
									__("Fix to Bottom of the Page", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"pl-fixedticker-bottom"
									),
								"description" => __("Choose News Ticker Position", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("News Ticker Height", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_height",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter News Ticker Height", __PW_POST_LAYOUT_TEXTDOMAN__ ),									
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("Border Size", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_border_size",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter Border Size", __PW_POST_LAYOUT_TEXTDOMAN__ ),									
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("Border Radius", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_border_radius",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter Border Radius", __PW_POST_LAYOUT_TEXTDOMAN__ ),									
							),
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Border Type", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_border_type",
								"value" =>array(
									__("Solid", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"solid",
									__("Dashed", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"dashed",
									__("Dotted", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"dotted",
									__("Double", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"double"
									),
								"description" => __("Choose Border Type", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => __("Border Color", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_border_color",
								"value" => '',
								"description" => __("Choose Border Color.Leave blank to ignore", __PW_POST_LAYOUT_TEXTDOMAN__ ),							
							),	
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Direction", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_direction",
								"value" =>array(
									__("LTR", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"pl-ltr",
									__("RTL", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"pl-rtl"
									),
								"description" => __("Choose Direction Type", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "pw_fontawesome",
								"holder" => "div",
								"class" => "",
								"heading" => __("Icon", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_icon",
								"description" => __("Choose Icon.Icon will be display with News Ticker title", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => __("Title Backgroud", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_title_background",
								"value" => '',
								"description" => __("Choose Backgroud of Title Color.Leave blank to ignore", __PW_POST_LAYOUT_TEXTDOMAN__ ),								
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("Title Width", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_title_width",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter News Ticker Title Width", __PW_POST_LAYOUT_TEXTDOMAN__ ),									
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("Title Font Size", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_title_fontsize",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter News Ticker Font Size for title", __PW_POST_LAYOUT_TEXTDOMAN__ ),								
							),
							
							
							array(
								'type' => 'checkbox',
								'heading' => __( 'Show Post Image', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'param_name' => 'pw_marquee_post_image',
								'description' => __( 'Display Post Image.', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'value' => array( __( 'Yes, please', __PW_POST_LAYOUT_TEXTDOMAN__ ) => 'yes' ),
								"holder" => "div",
							),
							
							
							array(
								"type" => "attach_image",
								"holder" => "div",
								"class" => "",
								"heading" => __("Contetn Backgroud Image", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_background_image",
								"value" => '',
								"description" => __("Choose Contetn Backgroud Image.", __PW_POST_LAYOUT_TEXTDOMAN__ )
							),	
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Background Repeat",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_background_repeat",
								"value" =>array(
									__("No Repeat", __PW_POST_LAYOUT_TEXTDOMAN__ ) =>"no-repeat",
									__("Repeat All", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"repeat",
									__("Repeat Horizontally", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"repeat-x",
									__("Repeat Vertically", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"repeat-y",
									__("Inherit", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"inherit",
									),
								"description" => __("Choose Background Repeat", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Background Attachment",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_background_attachment",
								"value" =>array(
									__("Fixed", __PW_POST_LAYOUT_TEXTDOMAN__ ) =>"fixed",
									__("Scroll", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"scroll",
									__("Inherit", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"inherit",
									),
								"description" => __("Choose Background Attachment", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("Background Position",__PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_background_position",
								"value" =>array(
									__("Left Top", __PW_POST_LAYOUT_TEXTDOMAN__ ) =>"left top",
									__("Left Center", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"left center",
									__("Left Bottom", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"left bottom",
									__("Center Top", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"center top",
									__("Center Center", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"center center",
									__("Center Bottom", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"center bottom",
									__("Right Top", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"right toper",
									__("Right Center", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"right center",
									__("Right Bottom", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"right bottom",
									
									),
								"description" => __("Choose Background Position", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),

							array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => __("Contetn Backgroud", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_content_background",
								"value" => '',
								"description" => __("Choose Backgroud of Content Color.Leave blank to ignore", __PW_POST_LAYOUT_TEXTDOMAN__ ),								
							),
							array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => __("Text Color", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_text_colour",
								"value" => '',
								"description" => __("Choose Excerpt Color.Leave blank to ignore", __PW_POST_LAYOUT_TEXTDOMAN__ ),								
							),		
							array(
								"type" => "colorpicker",
								"holder" => "div",
								"class" => "",
								"heading" => __("Content Hover Color", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_text_colour_hover",
								"value" => '',
								"description" => __("Choose Content text hover Color.Leave blank to ignore", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							array(
								"type" => "pw_number",
								"holder" => "div",
								"class" => "",
								"heading" => __("Content Font Size", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_marquee_content_fontsize",
								"value" => '',
								"suffix" =>"",
								"min"=>"0",
								"description" => __("Enter News Ticker Content Font Size", __PW_POST_LAYOUT_TEXTDOMAN__ ),							
							),
							array(
								'type' => 'checkbox',
								'heading' => __( 'Show Date', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'param_name' => 'pw_marquee_content_showdate',
								'description' => __( 'Display Date.', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'value' => array( __( 'Yes, please', __PW_POST_LAYOUT_TEXTDOMAN__ ) => 'yes' ),
								"holder" => "div",
							),
							array(
								"type" => "textfield",
								"holder" => "div",
								"class" => "",
								"heading" => __("Date Format", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_date_format",
								"value" => '',
								"description" => __("Enter Date Format.Show More Info <a href='http://codex.wordpress.org/Formatting_Date_and_Time' target='_new'>Here</a>", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'dependency' => array(
									'element' => 'pw_marquee_content_showdate',
									'value' => array( 'yes' )
								)	
							),

							/*  marquee - Slider - Vcarousel - Hcarousel  */
							array(
								"type" => "dropdown",
								"holder" => "div",
								"class" => "",
								"heading" => __("speed", __PW_POST_LAYOUT_TEXTDOMAN__ ),
								"param_name" => "pw_speed",
								"value" =>array(
									__("1 Second", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"1000",
									__("2 Seconds", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"2000",
									__("3 Seconds", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"3000",
									__("4 Seconds", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"4000",
									__("5 Seconds", __PW_POST_LAYOUT_TEXTDOMAN__ )=>"5000"
									),
								"description" => __("Choose Speed motion", __PW_POST_LAYOUT_TEXTDOMAN__ ),
							),
							
							array(
								'type' => 'checkbox',
								'heading' => __( 'Hide prev/next buttons', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'param_name' => 'pw_slider_hide_prev_next_buttons',
								'description' => __( 'If "YES" prev/next control will be removed.', __PW_POST_LAYOUT_TEXTDOMAN__ ),
								'value' => array( __( 'Yes, please', __PW_POST_LAYOUT_TEXTDOMAN__ ) => 'yes' ),
								"holder" => "div",
							)
						)
			) );
?>