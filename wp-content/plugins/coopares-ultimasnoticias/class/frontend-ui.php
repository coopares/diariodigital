<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if (!class_exists('VC_PW_marquee')) {
	class VC_PW_marquee extends WPBakeryShortCode_VC_Posts_Grid {
		public  $pw_title,
				$pw_query,
				$pw_link_target,
				$pw_marquee_type,
				$pw_marquee_position,
				$pw_marquee_height,
				$pw_marquee_border_size,
				$pw_marquee_border_radius,
				$pw_marquee_border_type,
				$pw_marquee_border_color,
				$pw_marquee_direction,
				$pw_marquee_title_fontsize,
				$pw_marquee_title_width,
				$pw_marquee_title_background,
				$pw_marquee_content_background,
				$pw_marquee_content_fontsize,
				$pw_marquee_content_showdate,
				$pw_date_format,
				$pw_marquee_text_colour,
				$pw_marquee_text_colour_hover,
				$pw_marquee_icon,
				$pw_speed,
				$pw_slider_hide_prev_next_buttons,
				
				$pw_marquee_background_image,
				$pw_marquee_background_repeat,
				$pw_marquee_background_attachment,
				$pw_marquee_background_position,
				$pw_marquee_post_image,
				$pw_mar_id;				
		function __construct($pw_title,
							$pw_query,
							$pw_link_target,
							$pw_marquee_type,
							$pw_marquee_position,
							$pw_marquee_height,
							$pw_marquee_border_size,
							$pw_marquee_border_radius,
							$pw_marquee_border_type,
							$pw_marquee_border_color,
							$pw_marquee_direction,
							$pw_marquee_title_fontsize,
							$pw_marquee_title_width,
							$pw_marquee_title_background,
							$pw_marquee_content_background,
							$pw_marquee_content_fontsize,
							$pw_marquee_content_showdate,
							$pw_date_format,
							$pw_marquee_text_colour,
							$pw_marquee_text_colour_hover,
							$pw_marquee_icon,
							$pw_speed,
							$pw_slider_hide_prev_next_buttons,
							$pw_marquee_background_image,
							$pw_marquee_background_repeat,
							$pw_marquee_background_attachment,
							$pw_marquee_background_position,
							$pw_marquee_post_image
						) {
			
			$this->pw_title=$pw_title;
			$this->pw_query=$pw_query;
			$this->pw_link_target=$pw_link_target;
			$this->pw_marquee_type=$pw_marquee_type;
			$this->pw_marquee_position=$pw_marquee_position;
			$this->pw_marquee_height=$pw_marquee_height;
			$this->pw_marquee_border_size=$pw_marquee_border_size;
			$this->pw_marquee_border_radius=$pw_marquee_border_radius;
			$this->pw_marquee_border_type=$pw_marquee_border_type;
			$this->pw_marquee_border_color=$pw_marquee_border_color;
			$this->pw_marquee_direction=$pw_marquee_direction;
			$this->pw_marquee_title_fontsize=$pw_marquee_title_fontsize;
			$this->pw_marquee_title_width=$pw_marquee_title_width;
			$this->pw_marquee_title_background=$pw_marquee_title_background;
			$this->pw_marquee_content_background=$pw_marquee_content_background;
			$this->pw_marquee_content_fontsize=$pw_marquee_content_fontsize;
			$this->pw_marquee_content_showdate=$pw_marquee_content_showdate;
			$this->pw_date_format=$pw_date_format;
			$this->pw_marquee_text_colour=$pw_marquee_text_colour;
			$this->pw_marquee_text_colour_hover=$pw_marquee_text_colour_hover;
			$this->pw_marquee_icon=$pw_marquee_icon;
			$this->pw_speed=$pw_speed;
			$this->pw_slider_hide_prev_next_buttons=$pw_slider_hide_prev_next_buttons;
			
			$this->pw_marquee_background_image=$pw_marquee_background_image;
			$this->pw_marquee_background_repeat=$pw_marquee_background_repeat;
			$this->pw_marquee_background_attachment=$pw_marquee_background_attachment;
			$this->pw_marquee_background_position=$pw_marquee_background_position;
			$this->pw_marquee_post_image=$pw_marquee_post_image;
												
			$this->pw_front_end();
			$this->pl_marquee_custom_color();
		}
		
		function pw_front_end()
		{
			global $output;
			$this->pw_mar_id = rand(0,1000);
			$loop=$this->pw_query;
			$grid_link = $grid_layout_mode = $title = $filter= '';
			$posts = array();
			if(empty($loop)) return;
			$this->getLoop($loop);
			$my_query = $this->query;
			$args = $this->loop_args;
			$img_id=array();
			$output = '';
			$img_counter = 0;
			$rand_id = rand(1,1000);
			
			$output .= '<div class="pl-tickercnt '.$this->pw_marquee_position.'" id="pl-ticker-'.$this->pw_mar_id.'">
                	<div class="pl-ticker-title '.$this->pw_marquee_direction.'"><i class="fa '.$this->pw_marquee_icon.'"></i>'.$this->pw_title.'</div>
                	<div class="pl-ticker-items '.$this->pw_marquee_direction.'">';
			$output .= '<ul class="pl-bxslider pl-box-car '.$this->pw_marquee_direction.'" id="marqueecar-'.$rand_id.'" >';
                   	  
			while ( $my_query->have_posts() ) {
				$my_query->the_post(); // Get post from query
				$post = new stdClass(); // Creating post object.
				$post->id = get_the_ID();
				$post->link = get_permalink($post->id);
				$this->setLinkTarget($this->pw_link_target);
				$img_id[]=get_post_meta( $post->id , '_thumbnail_id' ,true );
				$current_img_large = wp_get_attachment_image_src( $img_id[$img_counter++] , 'thumbnail' );
				
				$post_image = ($this->pw_marquee_post_image=='yes'?'<img class="pw-ticker-img '.$this->pw_marquee_direction.'" src="'.$current_img_large[0].'" alt="'.get_the_title().'" />':'');
				
				$output .= '<li>'.$post_image.'<a href="'.$post->link.'" target="'.$this->link_target.'">'.'<span class="'.($this->pw_marquee_content_showdate=='yes'?'pl-dated-title':'').'">'.get_the_title().'</span></a>'.($this->pw_marquee_content_showdate=='yes'?'<span class="pl-mar-date">'. get_the_date($this->pw_date_format).'</span>':'').'</li>';
			}
			wp_reset_query();
			$output .= '    </ul>
					</div><!--items -->
				    </div>';

			$output .= "<script type='text/javascript'>
            /* <![CDATA[  */ 
                jQuery(document).ready(function() {
                    slider" . $rand_id ." =
					 jQuery('#marqueecar-" . $rand_id ."').bxSlider({ 
						  mode : '".$this->pw_marquee_type."' ,
						  touchEnabled : true ,
						  adaptiveHeight : true ,
						  slideMargin : 10 , 
						  wrapperClass : 'pl-bx-wrapper pl-mar-car ".$this->pw_marquee_direction." ' ,
						  infiniteLoop:1,
						  pager:false,
						  controls:".($this->pw_slider_hide_prev_next_buttons=='yes'?'false':'true').",
						  minSlides: 1,
						  moveSlides: 1,
						  auto: true,
						  pause : ". $this->pw_speed."	,
						  autoHover  : true , 
 						  autoStart: true
					 });
					 jQuery('.pl-bx-wrapper .pl-bx-controls-direction a').click(function(){
						  slider" . $rand_id .".startAuto();
					 });
                });	
            </script>";
		}
		
		function pl_marquee_custom_color() {
			
			wp_enqueue_style('pw-pl-custom-style', PW_PS_PL_URL_BOXES . '/css/custom.css', array() , null); 
			
			$mar_height = $this->pw_marquee_height.'px ';
			$mar_border_size = $this->pw_marquee_border_size.'px ' . $this->pw_marquee_border_type . ' ' .$this->pw_marquee_border_color;
			$mar_border_radius = $this->pw_marquee_border_radius.'px ';
			
			$mar_title_back = $this->pw_marquee_title_background;
			$mar_title_width = $this->pw_marquee_title_width.'px ';
			$mar_title_font_size = $this->pw_marquee_title_fontsize.'px ';
			
			$mar_content_background = $this->pw_marquee_content_background;
			$mar_content_fontsize = $this->pw_marquee_content_fontsize.'px ';
			$mar_text_colour = $this->pw_marquee_text_colour;
			$mar_text_colour_hover = $this->pw_marquee_text_colour_hover;
			
			$current_img_large = wp_get_attachment_image_src( $this->pw_marquee_background_image , 'full' );
				
			$mar_content_background_img = 'url('.$current_img_large[0]. ') ';
			$mar_content_background_img_rep = $this->pw_marquee_background_repeat ;
			$mar_content_background_img_pos = $this->pw_marquee_background_position ;
			$mar_content_background_img_att = $this->pw_marquee_background_attachment ;
			
			$custom_css = '
				#pl-ticker-'.$this->pw_mar_id.'{ 
					height: '. $mar_height .';
					border: '.$mar_border_size.';
					-webkit-border-radius: '.$mar_border_radius.';
					-moz-border-radius: '.$mar_border_radius.';
					border-radius: '.$mar_border_radius.';
				}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title{
						-webkit-border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
						-moz-border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
						border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
					}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title.pl-rtl{
						-webkit-border-radius: 0 '.($mar_border_radius-2).'px  '.($mar_border_radius-2).'px 0;
						-moz-border-radius:  0 '.($mar_border_radius-2).'px  '.($mar_border_radius-2).'px 0;
						border-radius:  0 '.($mar_border_radius-2).'px  '.($mar_border_radius-2).'px 0;
					}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items{
						-webkit-border-radius: 0 '.($mar_border_radius-2).'px '.($mar_border_radius-2).'px 0 ;
						-moz-border-radius: 0 '.($mar_border_radius-2).'px '.($mar_border_radius-2).'px 0 ;
						border-radius: 0 '.($mar_border_radius-2).'px '.($mar_border_radius-2).'px 0 ;
					}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items.pl-rtl{
						-webkit-border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
						-moz-border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
						border-radius: '.($mar_border_radius-2).'px 0 0 '.($mar_border_radius-2).'px;
					}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title , #pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title i , #pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items  ul{ 
					line-height: '. $mar_height .';
				}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title{ 
					line-height: '. $mar_height .';
				}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title.pl-ltr:after { 
						right:-'.($this->pw_marquee_height/2).'px;
						border-width: '.($this->pw_marquee_height/2).'px 0 '.($this->pw_marquee_height/2) . 'px '.($mar_height/2).'px;
						border-color: transparent transparent transparent '.$mar_title_back.';
						line-height: 0px;
						_border-color: #000000 #000000 #000000 '.$mar_title_back.';
					}
					
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title.pl-rtl:after { 
						left:-'.($this->pw_marquee_height/2).'px;
						border-width: '.($this->pw_marquee_height/2).'px '.($this->pw_marquee_height/2) . 'px '.($mar_height/2).'px 0;
						border-color: transparent '.$mar_title_back.' transparent transparent ;
						line-height: 0px;
						_border-color: #000000 #000000 #000000 '.$mar_title_back.';
					}
					
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title{
					width : '.$mar_title_width.';
					background : '.$mar_title_back.';
					font-size : '.$mar_title_font_size.';
				}
					#pl-ticker-'.$this->pw_mar_id.' .pl-mar-date{
						background : '.$mar_title_back.';
						-webkit-border-radius: '.$mar_border_radius.';
						-moz-border-radius: '.$mar_border_radius.';
						border-radius: '.$mar_border_radius.';
					}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-title{
					font-size : '.$mar_title_font_size.';
				}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items {
					background-color : '.$mar_content_background.';
					color : '.$mar_text_colour.';
					
				}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items .pl-mar-car.pl-ltr{
						padding-left:'.($this->pw_marquee_height/2).'px;
					}
					#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items .pl-mar-car.pl-rtl{
						padding-right:'.($this->pw_marquee_height/2).'px;
					}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items {
					background-image:'.$mar_content_background_img.';
					background-repeat: '.$mar_content_background_img_rep.';
					background-position:'.$mar_content_background_img_pos.';
					background-attachment: '.$mar_content_background_img_att.' ;
				}
				
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items ul li a{
					font-size: '.$mar_content_fontsize.';
					color : '.$mar_text_colour.';
				}
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items ul li a:hover{
					color : '.$mar_text_colour_hover.';
				}
				
				#pl-ticker-'.$this->pw_mar_id.' .pl-ticker-items ul li .pw-ticker-img{  
					width : '. ($this->pw_marquee_height-10).'px;
					height : '. ($this->pw_marquee_height-10).'px;
					border: 1px solid '. $this->pw_marquee_border_color .';
					-webkit-border-radius: '.($mar_border_radius).';
					-moz-border-radius: '.($mar_border_radius).';
					border-radius: '.($mar_border_radius).';
				}
				
				#pl-ticker-'.$this->pw_mar_id.' .pl-mar-car  .pl-bx-controls-direction a{
					width: '.$mar_content_fontsize.';
					height: '.$mar_height.';
				}
				#pl-ticker-'.$this->pw_mar_id.' .pl-mar-car  .pl-bx-prev:before , #pl-ticker-'.$this->pw_mar_id.' .pl-mar-car  .pl-bx-next:before{
					font-size : '.$mar_content_fontsize.';
					line-height: '.$mar_height.';
					color: '.$mar_text_colour.';
				}
				#pl-ticker-'.$this->pw_mar_id.' .pl-mar-car  .pl-bx-prev:hover:before , #pl-ticker-'.$this->pw_mar_id.' .pl-mar-car  .pl-bx-next:hover:before{
					color : '.$mar_text_colour_hover.';
				}
				';
			
			wp_add_inline_style( 'pw-pl-custom-style', $custom_css );
		}
	}	
}
	
?>
