<?php
/*
Plugin Name: Dolar
Plugin URI: www.coopares.org
Description: Trae cotizacion del dolar en todos sus tipos
Author: Nikete
Version: 1.0
Author URI: www.niketedm.com.ar
*/
?>
<?php
/**
 * Example Widget Class
 */
class dolar_widget extends WP_Widget {


  /** constructor -- name this the same as the class above */
  function dolar_widget() {
    parent::WP_Widget(false, $name = 'Widget de Dolar');	
  }

  /** @see WP_Widget::widget -- do not rename this */
  function widget($args, $instance) {	
    extract( $args );
    $title 		= apply_filters('widget_title', $instance['title']);
    $message 	= $instance['message'];
    $api_url = 'https://www.kimonolabs.com/api/eex9ipes?apikey=MkhO58mGlB2xemb8wNMErSYwVOfPSkE8';
    $response = wp_remote_get( $api_url );
    $dolar = json_decode($response['body']);
?>
    <?php echo $before_widget; ?>
    <?php if ( $title )
    echo $before_title . $title . $after_title; ?>
    <ul>
      <li><?php echo $message; ?></li>
    </ul>
    <?php
    echo '<div class="row-fluid"><div class="span8"><ul>';
      foreach ( $dolar->results->LNdolarHoy1 as $oper )
      {
        echo '<li class="operacion-lista"><span class="operacion">';
          echo "{$oper->OPERACION}\n";
          echo  '</span></li>';
      }
      echo '</ul></div>';

      echo '<div class="span4"><ul>';
      foreach ( $dolar->results->LNdolarHoy1 as $opera )
      {
        echo '<li><span class="cotizacion">$';
          echo "{$opera->VALOR}\n";
          echo  '</span></li>';
      }
      echo '</ul></div>';
      echo '</div>';
    ?>
    <!--?php
    echo '<div class="row-fluid"><div class="span8"><ul>';
      foreach ( $dolar->results->LNdolarHoy2 as $oper )
      {
        echo '<li>';
          echo "{$oper->TIPO}\n";
          echo  '</li>';
      }
      echo '</ul></div>';

      echo '<div class="span4"><ul>';
      foreach ( $dolar->results->LNdolarHoy2 as $opera )
      {
        echo '<li>';
          echo "{$opera->VALOR}\n";
          echo  '</li>';
      }
      echo '</ul></div>';
      echo '</div>';
    ?-->

    <?php echo $after_widget; ?>
    <?php
  }

  /** @see WP_Widget::update -- do not rename this */
  function update($new_instance, $old_instance) {		
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['message'] = strip_tags($new_instance['message']);
    return $instance;
  }

  /** @see WP_Widget::form -- do not rename this */
  function form($instance) {	

    $title 		= esc_attr($instance['title']);
    $message	= esc_attr($instance['message']);
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('message'); ?>"><?php _e('Mensaje'); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>" type="text" value="<?php echo $message; ?>" />
    </p>
    <?php 
  }


} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("dolar_widget");'));
?>