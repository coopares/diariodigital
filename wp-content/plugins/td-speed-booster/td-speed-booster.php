<?php
/*
	Plugin Name: tagDiv speed booster
	Plugin URI: http://tagdiv.com
	Description: Speed booster for Newspaper theme - It moves the styles and all the scripts of the theme to the bottom. It also activates internal theme optimizations for better speed.
	Author: tagDiv
	Version: 2.5
	Author URI: http://tagdiv.com
*/

/*
 * 2.6  - fixed rendering bug on the loading of the page
 * 2.5  - code improvements, newspaper 4 compatibility
 *      - makes most of the javascript files use defer parsing
 *      - better compatibility with revolution slider
 * 2.4 - updated jquery version
 *     - support for https
 * 2.3 - fixed warnings when trying to move javascript files that are not registered
 */

class td_speed_booster {

    var $load_contact_form_7 = '';
    var $load_google_fonts = '';

    var $styles_for_footer = array();


    var $async_js_scripts = array(
        'contact-form-7',
        'bbpress',
        'woocommerce',
        'site',
        'devicepx',
        'js_composer_front'
    );

    function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts_hook'), 100);
        add_action('wp_footer', array($this, 'wp_footer_hook'), 15);
        add_action('wp_head', array($this, 'wp_head_hook'), 15);

        define('TD_SPEED_BOOSTER' , 'v2.5');
    }

    function enqueue_scripts_hook() {
        global $wp_scripts;

        //$this->move_js_to_footer('bp-legacy-js');




        //detect revmin - revolution slider and do not move jquery
        if( !is_admin() and !isset($wp_scripts->registered['revmin'])){
            if (is_ssl()) {
                $td_protocol = 'https';
            } else {
                $td_protocol = 'http';
            }

            wp_deregister_script('jquery');
            wp_register_script('jquery', ($td_protocol . '://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'), true, '1.11.1', true);
            wp_enqueue_script('jquery');
        }


        //move styles
        $this->move_style_to_footer('bbp-default-bbpress');  //bpress old
        $this->move_style_to_footer('bbp-default');  //bpress

        $this->move_style_to_footer('contact-form-7');
        $this->move_style_to_footer('td-bootstrap');
        $this->move_style_to_footer('td-theme');

        $this->move_style_to_footer('google-font-rest');
        $this->move_style_to_footer('woocommerce_frontend_styles');

        //jetpack lost styles
        $this->move_style_to_footer('jetpack-subscriptions');
        $this->move_style_to_footer('jetpack-widgets');


        $this->move_js_to_footer('themepunchtools');
        $this->move_js_to_footer('revmin');

        //bbpress
        //remove_action( 'bbp_head', array($BBP_Default, 'head_scripts') );
    }


    function wp_footer_hook() {

        $current_theme_version = '';

        if (defined('TD_THEME_VERSION')) {
            //get the theme version for style
            $current_theme_version = TD_THEME_VERSION;

            //on demo mode, autogenerate version hourly + day
            if (TD_DEPLOY_MODE == 'demo') {
                $current_theme_version = date('jG');
            }
        }

        foreach ($this->styles_for_footer as $style_id => $style_src) {
            echo "<link rel='stylesheet' id='" . $style_id . "-css'  href='" . $style_src . "?ver=" . $current_theme_version . "' type='text/css' media='all' />\n";
        }
    }

    function wp_head_hook() {
        if (defined('TD_THEME_VERSION')) {
            echo '<style>body {visibility:hidden;}</style>';
        }
    }


    function move_style_to_footer($style_id) {
        global $wp_styles;
        if (!empty($wp_styles->registered[$style_id]) and !empty($wp_styles->registered[$style_id]->src)) {
            $this->styles_for_footer[$style_id] = $wp_styles->registered[$style_id]->src;
            wp_deregister_style($style_id);
        }
    }

    function move_js_to_footer($js_id) {
        global $wp_scripts;
        if (isset($wp_scripts->registered[$js_id])) {
            wp_enqueue_script($js_id, ($wp_scripts->registered[$js_id]->src), '', $wp_scripts->registered[$js_id]->ver, true);
        }

    }


    function async_js_hook($url) {

        //check to see if we have js
        if (strpos( $url, '.js' ) === false) {
            return $url;
        }


        if ($this->strpos_array($url, $this->async_js_scripts) === false) {
            return $url;
        } else {
            return "$url' defer='defer";
        }


    }




    private function strpos_array($haystack_string, $needle_array, $offset=0) {
        foreach($needle_array as $query) {
            if(strpos($haystack_string, $query, $offset) !== false) {
                return true; // stop on first true result
            }
        }
        return false;
    }


}


new td_speed_booster();

