<?php
	/*
	Plugin Name: Clima Coopares
	Plugin URI: http://www.coopares.org
	Description: Clima de Rosario
	Version: 1.0
	Author: Nikete
	Author URI: http://www.niketedm.com.ar
	*/

	if(!defined('URL_BONOBO_PLUGIN_DIR')) define('URL_BONOBO_PLUGIN_DIR', plugins_url('',__FILE__));
	if(!defined('ABS_BONOBO_PLUGIN_DIR')) define('ABS_BONOBO_PLUGIN_DIR', dirname(__FILE__).'/');
	
	include_once(ABS_BONOBO_PLUGIN_DIR.'weather.php');
	
	class BONOBO_Weather extends WP_Widget
	{
		function BONOBO_Weather() 
		{
			$widget_ops = array('classname' => 'bonobo_weather', 'description' => '' );
			$this->WP_Widget('BONOBO_Weather', 'Clima', $widget_ops);
		}

		function form($instance) 
		{
?>
			<div style="width:100%;">
				<div style="text-align:center;"><strong>Configuracion</strong></div>
				<p>
					<input placeholder="Titulo" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title'); ?>" style="width:96%;" value="<?php echo $instance['title']; ?>"/>
				</p>
				<p>
					<input placeholder="Ciudad" id="<?php echo $this->get_field_id('city');?>" name="<?php echo $this->get_field_name('city'); ?>" style="width:96%;" value="<?php echo $instance['city']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('woeid'); ?>">WOEID <code><a href="http://woeid.factormystic.net/" target="_blank">get id</a></code>:</label><br/>
					<input placeholder="WOEID" id="<?php echo $this->get_field_id('woeid');?>" name="<?php echo $this->get_field_name('woeid'); ?>" style="width:96%;" value="<?php echo $instance['woeid']; ?>"/>
				</p>
				<br/>
				<div style="text-align:center;"><strong>Traducciones</strong></div>
				<p>
					<label for="<?php echo $this->get_field_id('today_text'); ?>">Text 'Today':</label><br/>
					<input placeholder="Today" id="<?php echo $this->get_field_id('today_text');?>" name="<?php echo $this->get_field_name('today_text'); ?>" style="width:96%;" value="<?php echo $instance['today_text']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('tomorrow_text'); ?>">Text 'Tomorrow':</label><br/>
					<input placeholder="Tomorrow" id="<?php echo $this->get_field_id('tomorrow_text');?>" name="<?php echo $this->get_field_name('tomorrow_text'); ?>" style="width:96%;" value="<?php echo $instance['tomorrow_text']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('humidity_text'); ?>">Text 'Humidity':</label><br/>
					<input placeholder="Humidity" id="<?php echo $this->get_field_id('humidity_text');?>" name="<?php echo $this->get_field_name('humidity_text'); ?>" style="width:96%;" value="<?php echo $instance['humidity_text']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('wind_text'); ?>">Text 'Wind':</label><br/>
					<input placeholder="Wind" id="<?php echo $this->get_field_id('wind_text');?>" name="<?php echo $this->get_field_name('wind_text'); ?>" style="width:96%;" value="<?php echo $instance['wind_text']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('pressure_text'); ?>">Text 'Pressure':</label><br/>
					<input placeholder="Pressure" id="<?php echo $this->get_field_id('pressure_text');?>" name="<?php echo $this->get_field_name('pressure_text'); ?>" style="width:96%;" value="<?php echo $instance['pressure_text']; ?>"/>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('file'); ?>">Title of the file translation:</label><br/>
					<input placeholder="File title" id="<?php echo $this->get_field_id('file');?>" name="<?php echo $this->get_field_name('file'); ?>" style="width:96%;" value="<?php echo $instance['file']; ?>"/><br/>
					<p class="description">This file is in folder with plugin. To create a new translation you must:</p><ul style="padding-left:15px;"><li><span class="description">copy translate.php</span></li><li><span class="description">rename it</span></li><li><span class="description">change text in new file</span></li></ul>
				</p>
				<br/>
				<div style="text-align:center;"><strong>Valores</strong></div>
				<p>
					<label for="<?php echo $this->get_field_id('color_icon'); ?>">Color de icono:</label><br/>
					<select name="<?php echo $this->get_field_name('color_icon'); ?>" style="width:100%">
						<option value="black" <?php if($instance['color_icon'] == 'black') echo 'selected'; ?>>black</option>
						<option value="white" <?php if($instance['color_icon'] == 'white') echo 'selected'; ?>>white</option>
						<option value="blue" <?php if($instance['color_icon'] == 'blue') echo 'selected'; ?>>blue</option>
						<option value="pink" <?php if($instance['color_icon'] == 'pink') echo 'selected'; ?>>pink</option>
						<option value="orange" <?php if($instance['color_icon'] == 'orange') echo 'selected'; ?>>orange</option>
						<option value="green" <?php if($instance['color_icon'] == 'green') echo 'selected'; ?>>green</option>
						<option value="dark_grey" <?php if($instance['color_icon'] == 'dark grey') echo 'selected'; ?>>dark grey</option>
						<option value="light_grey" <?php if($instance['color_icon'] == 'light grey') echo 'selected'; ?>>light grey</option>
					</select>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('color_navigation'); ?>">Color navegacion:</label><br/>
					<select name="<?php echo $this->get_field_name('color_navigation'); ?>" style="width:100%">
						<option value="white" <?php if($instance['color_navigation'] == 'white') echo 'selected'; ?>>white</option>
						<option value="black" <?php if($instance['color_navigation'] == 'black') echo 'selected'; ?>>black</option>
					</select>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('color_background'); ?>">Color fondo:</label><br/>
					<select name="<?php echo $this->get_field_name('color_background'); ?>" style="width:100%">
						<option value="0" <?php if($instance['color_background'] == '0') echo 'selected'; ?>>none</option>
						<option value="3" <?php if($instance['color_background'] == '3') echo 'selected'; ?>>green</option>
						<option value="4" <?php if($instance['color_background'] == '4') echo 'selected'; ?>>purple</option>
						<option value="1" <?php if($instance['color_background'] == '1') echo 'selected'; ?>>white</option>
						<option value="2" <?php if($instance['color_background'] == '2') echo 'selected'; ?>>black</option>
					</select>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('opacity_background'); ?>">Opacidad de fondo:</label><br/>
					<select name="<?php echo $this->get_field_name('opacity_background'); ?>" style="width:100%">
						<option value="20" <?php if($instance['opacity_background'] == '20') echo 'selected'; ?>>20%</option>
						<option value="40" <?php if($instance['opacity_background'] == '40') echo 'selected'; ?>>40%</option>
						<option value="60" <?php if($instance['opacity_background'] == '60') echo 'selected'; ?>>60%</option>
						<option value="80" <?php if($instance['opacity_background'] == '80') echo 'selected'; ?>>80%</option>
						<option value="100" <?php if($instance['opacity_background'] == '100') echo 'selected'; ?>>100%</option>
					</select>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('color_text'); ?>">Color de texto:</label><br/>
					<select name="<?php echo $this->get_field_name('color_text'); ?>" style="width:100%">
						<option value="white" <?php if($instance['color_text'] == 'white') echo 'selected'; ?>>white</option>
						<option value="#1a1a1a" <?php if($instance['color_text'] == '#1a1a1a') echo 'selected'; ?>>black</option>
					</select>
				</p>
				<p>
					<label for="<?php echo $this->get_field_id('transfer'); ?>">Grados:</label><br/>
					<select name="<?php echo $this->get_field_name('transfer'); ?>" style="width:100%">
						<option value="c" <?php if($instance['transfer'] == 'c') echo 'selected'; ?>>c</option>
						<option value="f" <?php if($instance['transfer'] == 'f') echo 'selected'; ?>>f</option>
					</select>
				</p>
			</div>
<?php 
		}

		function update($new_instance, $old_instance) 
		{
			return $new_instance;
		}

		function widget($args, $instance) 
		{
			extract( $args );
			if(file_exists(ABS_BONOBO_PLUGIN_DIR.$instance['file'].'.php')):
				include(ABS_BONOBO_PLUGIN_DIR.$instance['file'].'.php');
			else:
				include(ABS_BONOBO_PLUGIN_DIR.'translate.php');
			endif;
			echo $before_widget;
			if($instance['title'] != '') echo $before_title.$instance['title'].$after_title;
			$id = md5(mktime().rand());	
			
			try 
			{
			$w = new YahooWeather($instance['woeid'],$instance['transfer']);
?>
			<div id="bonobo_<?php echo $id;?>" class="bonobo_widget<?php if($instance['color_background'] != 0) echo ' color-'.$instance['color_background'];?><?php if($instance['opacity_background'] != '100') echo ' op'.$instance['opacity_background'];?>" style="color: <?php echo $instance['color_text'];?>;">
				<div class="left_navigation">
					<a href="#" rel="0"><img src="<?php echo URL_BONOBO_PLUGIN_DIR;?>/img/left_<?php echo $instance['color_navigation'];?>.png"/></a>
				</div>
				<div class="right_navigation">
					<a href="#" rel="2"><img src="<?php echo URL_BONOBO_PLUGIN_DIR;?>/img/right_<?php echo $instance['color_navigation'];?>.png"/></a>
				</div>
				<div class="center_content">
				<?php $i =1; foreach($w->forecast as $element) { ?>
					<div class="days day_<?php echo $i;?> <?php if($i!=1) echo 'invisible';?>">
						<div class="date">
							<?php switch($i)
								{
									case 1:
										{
											echo $instance['today_text'].' <span class="number_date">'.date('j/n',$element['date']).'</span>';
											break;
										}
									case 2:
										{
											echo $instance['tomorrow_text'].' <span class="number_date">'.date('j/n',$element['date']).'</span>';
											break;
										}
									default:
										{
											echo '<span class="number_date">'.date('j/n',$element['date']).'</span>';
											break;
										}
								}
							?>
						</div>
						<span class="city_title"><?php echo $instance['city']; ?><br/></span>
						<div class="icon">
						<?php 
							$icon_code = $element['code'];
							if($i == 1) $icon_code = $w->condition_code;
						?>
							<img src="<?php echo URL_BONOBO_PLUGIN_DIR.'/img/weather/'.$instance['color_icon'].'/'.bonobo_get_icon_url($icon_code);?>"/>
						</div>
						<div class="temperature">
							<?php echo bonobo_transfer_value($element['low'],$instance['transfer'],'temperature'); ?>&deg; / <?php echo bonobo_transfer_value($element['high'],$instance['transfer'],'temperature');?>&deg;
						</div>
						<ul class="desc">
							<li style="color: <?php echo $instance['color_text'];?>"><?php echo $text_array[$icon_code];?></li>
							<?php //if($i == 1 && $instance['humidity_text'] != ''): ?><!--li style="color: <?php echo $instance['color_text'];?>"><?php echo $instance['humidity_text'].' '.bonobo_transfer_value($w->humidity,'','humidity').'%';?></li--><?php //endif;?>
						</ul>
					</div>
				<?php $i++; } ?>
				</div>
			</div>
<?php				
			}catch(Exception $e) {
?>
			<div id="bonobo_<?php echo $id;?>" class="bonobo_widget<?php if($instance['color_background'] != 0) echo ' color-'.$instance['color_background'];?><?php if($instance['opacity_background'] != '100') echo ' op'.$instance['opacity_background'];?>" style="color: <?php echo $instance['color_text'];?>;">
				<div class="center_content">
					<?php echo $e->getMessage();?>
				</div>
			</div>
<?php
			}	
			echo $after_widget;
		}	
	}
	
	function upload_bonobo_widget_header_file()	
	{ 	
		
		wp_enqueue_script('jquery');
		
		wp_register_script( 'bonobo_js', URL_BONOBO_PLUGIN_DIR.'/js/bonobo.js',array('jquery'));
		wp_enqueue_script( 'bonobo_js' );	
	}
	
	function bonobo_get_icon_url($img)
		{
			if($img == 13) return '14.png';
			if($img == 14) return '13.png';
			if($img == 17) return '18.png';
			if($img == 18) return '10.png';
			if($img == 3200) return 'na.png';
			
			return $img.'.png';
		}
	
	function bonobo_transfer_value($value,$attribute,$type)
		{
			return $value;
		}
	add_action('wp_enqueue_scripts', 'upload_bonobo_widget_header_file');
	add_action('widgets_init', create_function('', 'return register_widget("BONOBO_Weather");'));
?>