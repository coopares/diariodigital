(function($, window, undefined){
	$(document).ready(function (){
		numItems = $('.bonobo_widget .days').length;
		next = parseInt($('.bonobo_widget .right_navigation a').attr('rel'));
		prev = next - 2;
		if(prev >= numItems) $('.right_navigation').addClass('invisible');
		if(prev <= 1) $('.left_navigation').addClass('invisible');
		for(var i = 1;i<=numItems;i++){
			$('.bonobo_widget .day_'+i).attr('style','min-width:'+$('.bonobo_widget .day_1').height()+'px;');
		}
		
		$('.bonobo_widget .right_navigation a').click(function(e){
			e.preventDefault();
			id =  $(this).parent().parent().attr("id");
			numItems = $('#'+id+' .days').length;
			next = parseInt($('#'+id+' .right_navigation a').attr('rel'));
			prev = parseInt($('#'+id+' .left_navigation a').attr('rel'));
			current = next - 1;
			
			$('#'+id+' .day_' + next).removeClass('invisible');
			$('#'+id+' .day_' + current).addClass('invisible');
			
			next = next + 1;
			prev = prev + 1;
			
			$('#'+id+' .right_navigation a').attr('rel',next);
			$('#'+id+' .left_navigation a').attr('rel',prev);
			
			if(prev >= 1) $('#'+id+' .left_navigation').removeClass('invisible');
			if(next > numItems) $('#'+id+' .right_navigation').addClass('invisible');
		});	
		$('.bonobo_widget .left_navigation a').click(function(e){
			e.preventDefault();
			id =  $(this).parent().parent().attr("id");
			numItems = $('#'+id+' .days').length;
			next = parseInt($('#'+id+' .right_navigation a').attr('rel'));
			prev = parseInt($('#'+id+' .left_navigation a').attr('rel'));
			current = prev + 1;
			
			$('#'+id+' .day_' + current).addClass('invisible');
			$('#'+id+' .day_' + prev).removeClass('invisible');
			
			next = next - 1;
			prev = prev - 1;
			
			$('#'+id+' .right_navigation a').attr('rel',next);
			$('#'+id+' .left_navigation a').attr('rel',prev);
			
			if(prev < 1) $('#'+id+' .left_navigation').addClass('invisible');
			if(next <= numItems) $('#'+id+' .right_navigation').removeClass('invisible');
			
		});	
	});
})(jQuery, window);