<?php
/*
Plugin Name: Cereales
Plugin URI: www.coopares.org
Description: Trae cotizacion
Author: Nikete
Version: 1.0
Author URI: www.niketedm.com.ar
*/

?>
<?php
/**
 * Example Widget Class
 */
class mercado_widget extends WP_Widget {


  /** constructor -- name this the same as the class above */
  function mercado_widget() {
    parent::WP_Widget(false, $name = 'Widget de Mercado');	
  }

  /** @see WP_Widget::widget -- do not rename this */
  function widget($args, $instance) {	
    extract( $args );
    $title 		= apply_filters('widget_title', $instance['title']);
    $message 	= $instance['message'];
    $api_url = 'https://www.kimonolabs.com/api/b1vafbii?apikey=MkhO58mGlB2xemb8wNMErSYwVOfPSkE8';
    $response = wp_remote_get( $api_url );
    $cereales = json_decode($response['body']);
?>
    <?php echo $before_widget; ?>
    <?php if ( $title )
    echo $before_title . $title . $after_title; ?>
    <ul>
      <li><?php echo $message; ?></li>
    </ul>
    <?php
    echo '<div class="row-fluid"><div class="span8"><ul><li><strong>Cereal</strong></li>';
      foreach ( $cereales->results->mercado as $cer )
      {
        echo '<li>';
          echo "{$cer->cereal}\n";
          echo  '</li>';
      }
      echo '</ul></div>';

      echo '<div class="span4"><ul><li><strong>Cotizacion</strong></li>';
      foreach ( $cereales->results->mercado as $cotiza )
      {
        echo '<li>';
          echo "{$cotiza->cotizacion}\n";
          echo  '</li>';
      }
      echo '</ul></div>';
      echo '</div>';
    ?>
    <?php echo $after_widget; ?>
    <?php
  }

  /** @see WP_Widget::update -- do not rename this */
  function update($new_instance, $old_instance) {		
    $instance = $old_instance;
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['message'] = strip_tags($new_instance['message']);
    return $instance;
  }

  /** @see WP_Widget::form -- do not rename this */
  function form($instance) {	

    $title 		= esc_attr($instance['title']);
    $message	= esc_attr($instance['message']);
    ?>
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id('message'); ?>"><?php _e('Mensaje'); ?></label> 
      <input class="widefat" id="<?php echo $this->get_field_id('message'); ?>" name="<?php echo $this->get_field_name('message'); ?>" type="text" value="<?php echo $message; ?>" />
    </p>
    <?php 
  }


} // end class example_widget
add_action('widgets_init', create_function('', 'return register_widget("mercado_widget");'));
?>